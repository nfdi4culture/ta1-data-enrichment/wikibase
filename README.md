# Wikibase

This repository contains configuration for running Wikibase locally, as well as resources for server deployment. 

### Requirements

Requirments for running this deployment are 

- [docker](https://www.docker.com/) (tested using v23.0.0)
- [docker compose](https://docs.docker.com/compose/) (tested using v2.5.0)  
- [jq](https://stedolan.github.io/jq/) (tested using jq-1.6). 

### Service

This deployment will generate the following docker containers:

| service | image | 
| ------ | ------ | 
| wikibase  |   wikibase/wikibase-bundle:1.38.5-wmde.10     | 
|    wikibase_jobrunner    |     wikibase/wikibase-bundle:1.38.5-wmde.10   |
| mysql | mariadb:10.9 |
| elasticsearch | wikibase/elasticsearch:6.8.23-wmde.10 |
| wdqs |  wikibase/wdqs:0.3.118-wmde.10 |
|  wdqs-proxy | wikibase/wdqs-proxy:wmde.10 |
|   wdqs-updater | wikibase/wdqs:0.3.118-wmde.10 |
| wdqs-frontend | custom-wdqs:latest | 

The `custom-wdqs` image is built using resources from [this repo](https://gitlab.com/nfdi4culture/ta1-data-enrichment/custom-wdqs).

### Extensions

By default the following extensions are enabled: 
| extension | version |
| ------ | ------ |
| Babel| 1.12.0|
|CirrusSearch| 6.5.4 |
|CLDR|4.11.0|
|  ConfirmAccount      |    f30eace    |
|ConfirmEdit|	1.6.0|
|Elastica| 6.2.0|
|EntitySchema| - |
|    Nuke    |   -     |
| OAuth |  1.1.0 |
|Parsoid| - |
| Scribunto |  -|
|SyntaxHighlight| 2.0 |
|UniversalLanguageSelector| 2022-01-19|
|VisualEditor | 0.1.2|
|WikibaseCirrusSearch| 0.1.0|
|WikibaseClient | - |
|WikibaseEDTF|2.0.1|
|WikibaseLocalMedia|1.0.2|
| WikibaseManifest|  0.0.1 |
|WikibaseRDF| 1.1.0 |
|WikibaseRepository| - |

### Configuration

Configuring the instance can be performed via the `variables.env` file:

### Local

To deploy locally, git clone this repo, from within the `wikibase` directory run:
> ./run.sh 

If you encounter permission issues, You may need to first run  
>chmod +x run.sh

### Server

An example of configuring `variables.env` for server deployment could look like this:

```sh
# change project name, this will be displayed throughout the wikibase.
WIKIBASE_NAME="Wikibase Project"

# https likely
WIKIBASE_SCHEME=https

# address of wikibase once routed from port 8900 to domain
WIKIBASE_ADDRESS=wikibase.tibwiki.io

# address with scheme.
WIKIBASE_SERVER=https://wikibase.tibwiki.io

# address (this is repeated, due to being different for local)
WIKIBASE_CONCEPT=wikibase.tibwiki.io

# query service address once routed from port 8834
CUSTOM_CONFIG_QUERY=query.wikibase.tibwiki.io

# mailgun config expected
MAILGUN_USERNAME=
MAILGUN_PASSWORD=

# details for default admin account
MW_ADMIN_NAME=admin
MW_ADMIN_PASS=notaverygoodpassword
MW_ADMIN_EMAIL=admin@admin.com

# details for mariadb server
MARIA_NAME=my_wiki
MARIA_USER=admin
MARIA_PASSWORD=notaverygoodpassword

# below should remain unchanged
WIKIBASE_HOST=wikibase.svc
WDQS_HOST=wdqs.svc
WDQS_PORT=9999
PROXY_PASS_HOST=wdqs.svc:9999
WDQS_HOST_PROXY=wdqs-proxy.svc
WIKIBASE_PINGBACK=false
MW_SECRET_KEY=some-secret-key
MW_WG_SECRET_KEY=some-secret-key
MW_WG_ENABLE_UPLOADS=true
MARIA_SERVER=mysql.svc:3306
MARIA_RANDOM=yes
MW_ELASTIC_HOST=elasticsearch.svc
MW_ELASTIC_PORT=9200
MAX_JOBS=1
```

Once configured run the same `run.sh` script as above. Using the above configuration, wikibase should be routed to domin from `ip:8900` and query service from `ip:8834`.
