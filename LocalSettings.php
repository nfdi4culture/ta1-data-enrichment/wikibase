<?php

# custom sitename
$wgSitename = getenv('WIKIBASE_NAME');

# custom logo
$wgLogo = "$wgResourceBasePath/images/icon.png";

// # https config
// $wgServer = getenv('WIKIBASE_SERVER');
// $_SERVER['HTTPS'] = 'on';

# load localmedia extension
wfLoadExtension( 'WikibaseLocalMedia' );

# load rdf extension.
wfLoadExtension( 'WikibaseRDF' );

# allow file upload and thumbnail config.
$wgEnableUploads = true; 
$wgGenerateThumbnailOnParse = false;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = '/usr/bin/convert'; 
$wgThumbnailScriptPath = "$wgScriptPath/thumb.php";

# set smtp
$wgSMTP = [
    'host' => 'smtp.eu.mailgun.org',  
    'IDHost' => 'mailgun.org',
    'port' => 587,
    'username' => getenv('MAILGUN_USERNAME'),    
    'password' => getenv('MAILGUN_PASSWORD'), 
    'auth' => true
];

# load confirmaccount extension
wfLoadExtension( 'ConfirmAccount' );

# disable captchas on account creation
$wgCaptchaTriggers['createaccount'] = false;

# permissions on account notification
$wgGroupPermissions['sysop']['confirmaccount-notify'] = true;
$wgGroupPermissions['bureaucrat']['confirmaccount-notify'] = true;
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['edit'] = false;

# required for account creation
$wgConfirmAccountRequestFormItems = array(
 'UserName'        => array( 'enabled' => true ),
 'RealName'        => array( 'enabled' => false ),
 'Biography'       => array( 'enabled' => false, 'minWords' => 5 ),
 'AreasOfInterest' => array( 'enabled' => false ),
 'CV'              => array( 'enabled' => false ),
 'Notes'           => array( 'enabled' => false ),
 'Links'           => array( 'enabled' => false ),
 'TermsOfService'  => array( 'enabled' => false ),
);

// # permissive password generation
// $wgPasswordPolicy = [ 'policies' => [ 'default' => [] ], 'checks' => [] ];

// # load kompakktcachecleaner extension
// #wfLoadExtension( 'KompakktCacheCleaner' );

// # general rights text and link
// $wgRightsUrl = 'https://creativecommons.org/publicdomain/zero/1.0/';
// $wgRightsText = 'Creative Commons Zero License';
// $wgRightsPage = 'Project:Copyrights';

// # add annotations namespace for annotation data written from Kompakkt.
// define("NS_ANN", 3000);
// define("NS_ANN_TALK", 3001);
// $wgExtraNamespaces[NS_ANN] = "Annotation";
// $wgExtraNamespaces[NS_ANN_TALK] = "Annotation_talk";

// $wgContentNamespaces[] = NS_ANN;
// $wgNamespacesToBeSearchedDefault[NS_ANN] = true;
