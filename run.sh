#!/bin/bash

# variables

cp variables.env .env
set -a && source .env

# config for open refine endpoint

git clone https://gitlab.com/nfdi4culture/ta1-data-enrichment/openrefine-wikibase
chmod +x ./openrefine-wikibase/config/config.sh
./openrefine-wikibase/config/config.sh ./openrefine-config.py
chmod +x ./openrefine-wikibase/config/manifest.sh
./openrefine-wikibase/config/manifest.sh ./openrefine-manifest.json

# build custom query frontend

git clone https://gitlab.com/nfdi4culture/ta1-data-enrichment/custom-wdqs.git
chmod +x ./custom-wdqs/config/custom-config.sh
./custom-wdqs/config/custom-config.sh ./custom-wdqs/custom-config.json
chmod +x ./custom-wdqs/config/RdfNamespaces.sh 
./custom-wdqs/config/RdfNamespaces.sh ./custom-wdqs/wikibase/queryService/RdfNamespaces.js
chmod +x ./custom-wdqs/config/QueryHelper.sh 
./custom-wdqs/config/QueryHelper.sh ./custom-wdqs/wikibase/queryService/ui/queryHelper/QueryHelper.js
chmod +x ./custom-wdqs/config/index.sh 
./custom-wdqs/config/index.sh ./custom-wdqs/index.html
docker build ./custom-wdqs -t custom-wdqs

# generate prefixes config

chmod +x ./prefixes.sh
./prefixes.sh ./prefixes.conf

# add confirm account extension

EXTENSION=ConfirmAccount
RELEASE=1_39
TAR_URL=$(curl -s "https://www.mediawiki.org/w/api.php?action=query&list=extdistbranches&edbexts=$EXTENSION&formatversion=2&format=json" | jq -r ".query.extdistbranches.extensions.$EXTENSION.REL${RELEASE}")

if [ "$TAR_URL" = "null" ]; then
    echo "Error: Could not find extension $EXTENSION for REL$RELEASE"
    exit 1
fi
echo -n "Downloading $1 from $TAR_URL..."
curl -s "$TAR_URL" -o "$EXTENSION".tar.gz
echo " OK"
echo -n "Unpacking..."
tar xzf "$EXTENSION".tar.gz && rm "$EXTENSION".tar.gz
echo " OK"

# run wikibase

docker compose up -d

# install rdf extension

sleep 60
docker exec -it wikibase_wikibase bash -c 'curl -O  https://getcomposer.org/composer.phar'
docker exec -it wikibase_wikibase bash -c 'COMPOSER=composer.json php composer.phar require --no-update professional-wiki/wikibase-rdf:~1.0'
docker exec -it wikibase_wikibase bash -c 'php composer.phar update professional-wiki/wikibase-rdf --no-dev -o'
docker exec -it wikibase_wikibase bash -c 'php maintenance/update.php'

# increase file upload limit

docker exec -it wikibase_wikibase bash -c 'echo "php_value upload_max_filesize 50M" >> .htaccess'
docker exec -it wikibase_wikibase bash -c 'echo "php_value post_max_size 50M" >> .htaccess'

# fix search index

docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/UpdateSearchIndexConfig.php'
docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipLinks --indexOnSkip'
docker exec -it wikibase_wikibase bash -c 'php extensions/CirrusSearch/maintenance/ForceSearchIndex.php --skipParse'
docker exec -it wikibase_wikibase bash -c 'php maintenance/runJobs.php'

# copy icon

docker cp icon.png wikibase_wikibase:/var/www/html/images/icon.png
