#!/bin/bash
# generate prefixes.sh for wdqs.

echo "PREFIX psn: <http://www.wikidata.org/prop/statement/value-normalized/>
PREFIX pqn: <http://www.wikidata.org/prop/qualifier/value-normalized/>
PREFIX prn: <http://www.wikidata.org/prop/reference/value-normalized/>
PREFIX mwapi: <https://www.mediawiki.org/ontology#API/>
PREFIX gas: <http://www.bigdata.com/rdf/gas#>
PREFIX tib: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/entity/>
PREFIX tibt: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/direct/>
PREFIX tibp: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/>
PREFIX tibps: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/statement/>
PREFIX tibpq: <${WIKIBASE_SCHEME}://${WIKIBASE_CONCEPT}/prop/qualifier/>" > $1

echo "prefixes.conf processed"
